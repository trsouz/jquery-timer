(function ($) {

  var default_options = {
    alerts: [5, 10, 15, 20, 30, 60, 60*5, 60*10],
    classes: {
      component: 'timer',
      loading: 'loading',
      complete: 'complete',
      alert: 'alert',
      separator: '-',
      year: 'year',
      day: 'day',
      hour: 'hour',
      minute: 'minute',
      second: 'second',
      hundreds: 'hundreds',
      tens: 'tens',
      units: 'units'
    }
  };

  $.fn.timer = function (_params) {
    return getTimer.call(this, _params);
  };

  function getTimer(_params) {
    var master_self = this;
    var container_cb = [];
    var params = $.extend(true, {}, default_options, _params);

    var getClass = function(arg, dotted){
      dotted = (typeof(dotted) == 'undefined') ? true : false;
      return [dotted ? '.' : '',
        params.classes.component,
        params.classes.separator,
        params.classes[arg]].join('');
    };

    var insertValue = function(arg, value){
      var elem = this.find(getClass(arg));
      var values = value.split('').reverse();
      var hundreds = elem.find(getClass('hundreds'));
      var tens = elem.find(getClass('tens'));
      var units = elem.find(getClass('units'));

      if(hundreds.length || tens.length || units.length){
        units.text(values[0] || '0');
        tens.text(values[1] || '0');
        hundreds.text(values[2] || '0');
      }else{
        elem.text(value);
      }

      //TODO: Adicionar classe de completo para cada tag separada
      /*if(parseInt(value, 10) <= 0){
        elem.addClass(getClass('complete', false));
      }
      */
    };

    //TODO: terminar o callback. Não está funcionando.
    var complete = function(obj){
      for(var i in container_cb){
        var current = container_cb[i];
        if(typeof current == 'function'){
          current.call(master_self, obj);
        }
      }
    };

    // ordenando os alertas
    params.alerts = params.alerts.sort(function(a,b){return a-b});

    this.done = function(func){
      container_cb.push(func);
      return master_self;
    };

    return this.each(function () {
      var self = $(this);

      var attr_end = $(this).data('timer') ? ($(this).data('timer').match(/(\d{1,4})|(\d{1,2})/gi) || []) : [];

      var objs = {};
      $.each(['day', 'month', 'year', 'hours', 'minutes', 'seconds'], function (index, value) {
        objs[value] = attr_end.slice(index, index + 1).length ? attr_end.slice(index, index + 1)[0] : '';
      });

      var checkTime = function (i) {
        if (i < 10 && i >= 0) {
          i = "0" + i;
        } else if (isNaN(i) || i < 0) {
          i = "00";
        }
        return i + '';
      };

      var changeDate = function () {
        var date_now = new Date();
        //TODO: O mês ainda está passível de erro;
        var date_end = new Date(
          (objs.year || ''),
          ((parseInt(objs.month, 10) - 1) || ''),
          (objs.day || ''),
          (objs.hours || ''),
          (objs.minutes || ''),
          (objs.seconds || '')
        );

        // obter o tempo total em seconds
        var date_difference = (date_end.getTime() - date_now.getTime()) / 1000;
        date_difference = Math.floor(date_difference);

        // obter os days
        var days = date_difference / 86400;
        days = Math.floor(days);
        days = checkTime(days);

        // obter as horas
        var hours = (date_difference - (days * 86400)) / 3600;
        hours = Math.floor(hours);
        hours = checkTime(hours);

        // obter os minutos
        var minutes = (date_difference - (days * 86400) - (hours * 3600)) / 60;
        minutes = Math.floor(minutes);
        minutes = checkTime(minutes);

        // obter os seconds
        var seconds = (date_difference - (days * 86400) - (hours * 3600) - (minutes * 60));
        seconds = checkTime(seconds);

        $.each({'day': days, 'hour': hours, 'minute': minutes, 'second': seconds}, function(a,b){
          insertValue.call(self, a, b);
        });

        $.each(params.alerts, function(a, b){
          var current_class = getClass('alert', false);
          if(date_difference <= b){
            self.addClass([current_class, b].join('-'));
            if(date_difference <= 0 || date_difference <= params.alerts[a - 1]){
              self.removeClass([current_class, b].join('-'));
            }
          }
        });

        if (date_difference > 0){
          setTimeout(function(){
            changeDate.call(self);
          }, 100);
        }else{
          self.addClass(getClass('complete', false));
        }
      };
      changeDate.call(self);
      self.removeClass(getClass('loading', false));
    });
  }
})(jQuery);